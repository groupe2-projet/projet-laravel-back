<?php
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Public routes
// Route::post('/registre', [AuthController::class, 'registre']);  
// Route::post('/login', [AuthController::class, 'login']);  
Route::get('/profiles', [ProfileController::class, 'index']);
Route::post('/profiles/{id}', [ProfileController::class, 'show']);
Route::get('/profiles/search/{titre}', [ProfileController::class, 'search']);

//Protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/profiles', [ProfileController::class, 'store']);
    Route::put('/profiles/{id}', [ProfileController::class, 'update']);
    Route::delete('/profiles/{id}', [ProfileController::class, 'destroy']);
    Route::post('/logout', [AuthController::class, 'logout']); 
    

});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
